#!/bin/bash

MACHINES="LXC1 LXC2 LXC3 LXC4"

echo -e "\n"
for var in ${MACHINES} 
do
	echo "*************************************************"
	echo -e " \ncreating ${var} ..."
	sudo lxc-create --name $var --template ubuntu --quiet
	echo "starting ${var} ..."
	sudo lxc-start --name $var --quiet
	echo -e "FINISHED! \n \nINFO:"
	sudo lxc-info $var
	echo -e "\n"
done
echo "*************************************************"

