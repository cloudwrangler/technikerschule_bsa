#!/bin/bash -eux

get_machine(){
	if ! sudo lxc-info ${1} >/dev/null 2>&1
	then 
		sudo lxc-create --name ${1} --template ubuntu
	fi
	if sudo lxc-info ${1} | grep -i stopped >/dev/null
	then 
		sudo lxc-start --name ${1}
		sleep 5
	fi
}

get_ip(){
	get_machine ${1}
	IP=$(sudo lxc-info --ips --no-humanize ${1})
}

ssh_lxc(){
	get_ip ${1}
	sshpass -p ubuntu ssh -o StrictHostKeyChecking=accept-new ubuntu@${IP}
}

#Hier fängt das Script an
if [ ! ${#} -eq 1 ]
then
	echo "use: ${0} <LXC Name>"
else
	ssh_lxc ${1}
fi

exit 0
