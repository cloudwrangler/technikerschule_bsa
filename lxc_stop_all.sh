#!/bin/bash

machines=$(lxc-ls --running)

for i in ${machines}
do
	echo machine ${i} wird gestoppt
	lxc-stop -n ${i}
done

exit 0

