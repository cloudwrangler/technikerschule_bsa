#!/bin/bash -x


PARAM=$1

for i in ${PARAM}
do
	if sudo lxc-info $i > /dev/null 2>&1
	then
		echo machine $i is existing
	else
		echo creating machine $i
		sudo lxc-create --name $i --template ubuntu
	fi

	if sudo lxc-info $i 2> /dev/null | grep -i stopped > /dev/null 2>&1
	then
		echo starting machine $i
		sudo lxc-start $i
		sleep 5s
	else
		echo machine $i is running
	fi
done
	_IPv4=$(sudo lxc-info $i | grep IP | rev | cut -d \  -f1 | rev)
	#_IPv4=$(sudo lxc-info $i | grep IP | cut --delimiter=":" -f2 | tr --delete " ")
	sudo ssh ubuntu@$_IPv4

exit 0
