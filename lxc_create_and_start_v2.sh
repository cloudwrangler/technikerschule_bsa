#!/bin/bash

PARAMETERS=$*

for VAR in ${PARAMETERS:=DefaultMachine} #Wenn Variable PARAMETERS leer ist, dass wird sie gefüllt mit "DefaultMachine"
do
	if ! sudo lxc-info $VAR > /dev/null 2>&1
	then
		echo creating machine $VAR
		sudo lxc-create --name $VAR --template ubuntu
	else
		echo machine $VAR already existing
	fi

	if sudo lxc-info $VAR 2> /dev/null | grep -i stopped > /dev/null 2>&1
	then
 		echo starting machine $VAR
		sudo lxc-start $VAR
		sleep 5s
	else
		echo machine $VAR is running
	fi

	sudo lxc-info $VAR -i
	#ODER:
	#sudo lxc-info $VAR | grep IP
	echo
done

exit 0
