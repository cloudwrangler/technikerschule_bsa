#!/bin/bash

for i in $*
do
	_result=$(apt-cache policy $i | grep -i installiert) 	#Überprüft ob Paket installiert ist #grep -i <-- case insensitive
        
        #debugging:
	echo "*********************************************************"
	echo 'var $_result =' "$_result"
	echo "*********************************************************"
	
	if [ -z "$_result" ] 					#If condition ob vorherige Abfrage leer ist
	then
		echo Packet $i existiert nicht
	else
		if apt-cache policy $i | grep -i keine >/dev/null 2>&1
		then
			echo Packet $i existiert, ist aber nicht installiert
		else
			echo Paket $i ist installiert
		fi
	fi
	echo "*********************************************************"
done
