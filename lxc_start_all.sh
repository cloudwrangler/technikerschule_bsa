#!/bin/bash

machines=$(lxc-ls --stopped)

for i in ${machines}
do
	echo machine ${i} wird gestartet
	lxc-start -n ${i}
done

exit 0

