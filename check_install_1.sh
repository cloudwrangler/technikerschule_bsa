#!/bin/bash
PARAM=$*
for PARAM  in ${PARAM}
do
	apt-cache policy $PARAM | grep '(keine)' > /dev/null 2>&1

        if [ $? == 0 ]
        then
                echo ist nicht installiert
	else
                apt-cache policy $PARAM | grep 'Installiert:' > /dev/null 2>&1
                if [ $? == 0 ]
                then
                        echo ist installiert
                else
                        echo paket gibt es nicht
                fi
        fi
done
