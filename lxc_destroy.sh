#!/bin/bash

MACHINES=$*

for i in ${MACHINES}
do
	info=$(sudo lxc-info --name ${i})
        #echo ${info}

	if echo "${info}" | grep -i stopped > /dev/null 2>&1
	then
		#echo "IF ${i}"
		sudo lxc-destroy --name ${i} > /dev/null 2>&1
		echo Machine ${i} is now destroyed...

	elif echo "${info}" | grep -i running > /dev/null 2>&1
	then
		#echo "ELSE IF ${i}"
		echo Machine ${i} is not stopped...
		sudo lxc-stop --name ${i} > /dev/null 2>&1
		echo Machine ${i} has been stopped...
		sudo lxc-destroy --name ${i} > /dev/null 2>&1
		echo Machine ${i} is now destoryed...
	
	else
		#echo "ELSE ${i}"
		echo Machine ${i} does not exist
	fi

	echo -e "\n"
done





