#!/bin/bash

MACHINES="LXC1 LXC2 LXC3 LXC4"

echo -e "\n"
for var in ${MACHINES} 
do
	echo "*************************************************"
	echo -e " \nstopping ${var} ..."
	sudo lxc-stop --name $var --quiet
	echo "destroying ${var} ..."
	sudo lxc-destroy --name $var --quiet
	echo -e "FINISHED! \n \nINFO:"
	sudo lxc-info $var
	echo -e "\n"
done
echo "*************************************************"

