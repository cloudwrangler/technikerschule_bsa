#!/bin/bash

PARAM=$(cat ./file.txt)

echo "Script is running..."
for i in ${PARAM}
do
	if sudo lxc-info -n ${i} > /dev/null 2>&1
	then
		echo Machine ${i} already exists...
	else
		sudo lxc-create --name ${i} --template ubuntu --quiet
		echo Machine ${i} was created...
	fi

	if sudo lxc-info -n ${i} | grep -i stopped > /dev/null 2>&1
	then
		sudo lxc-start --name ${i} --quiet
		echo Machine ${i} was started...
	else
		echo Machine ${i} is already started...
	fi

	sleep 5
	IP=$(sudo lxc-info --name ${i} --ips --no-humanize)
	echo "Machine ${i}'s IP-Address is: ${IP}"
	
	sshpass -p ubuntu ssh -o StrictHostKeyChecking=accept-new -t ubuntu@${IP} "mkdir PrivateDataFrom${i}; echo 'executed correctly'; exit"
done
echo "Script finished!"
